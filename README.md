### Motivation

Modern operating systems often do not support older devices. For instance neither Windows 10 nor macOS Catalina have scanner drivers for an excellent HP LaserJet M1120 MFP:

![HP LaserJet M1120 MFP](resources/hp_laserjet_m1120_mfp.jpg)

Fortunately Linux does support the device so I put together this VM with [Simple Scan](https://launchpad.net/simple-scan) inside to be able to scan documents on Windows and macOS.

### Requirements

Software dependencies:

- Vagrant 2.2.9
- VirtualBox 6.1.12
- VirtualBox extension pack 6.1.12

### Usage instructions

- Spin up the VM using Vagrant:

  ```
  vagrant up
  ```

- Setup a USB filter to make the scanner available in the VM (Setting > Ports > USB > USB Device Filters):

  ![New USB filter in VirtualBox](resources/usb_filter.png)

- Reboot the VM via `vagrant halt` followed by `vagrant up`. After the restart you should see graphical UI.

- Login with username "vagrant" and password "vagrant"

- Launch Simple Scan

### Troubleshooting tips

If for some reason Simple Scan cannot contact your scanner you can examine output of `lsusb` and `scanimage -L`. `scanimage` is a command line utility that allows to scan documents.